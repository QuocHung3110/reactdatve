import React, { Component } from "react";
import { connect } from "react-redux";

class Modal extends Component {
  render() {
    let { image, name, price, description, shortDescription } =
      this.props.detail;
    return (    
      <div>
        <div className="modal" tabIndex={-1} role="dialog" id="myModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">{name}</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <img src={image} style={{width:300}}/>
                <p>${price}</p>
                <p>{description}</p>
                <p>{shortDescription}</p>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) =>{
  return {
    detail: state.shoesShopReducer.detail,
  }
}
export default connect (mapStateToProps)(Modal)
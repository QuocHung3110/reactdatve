import React, { Component } from "react";
import { connect } from "react-redux";

class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.shoe;
    return (
      <div className="col-4 mb-4">
        <div className="card">
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">$ {price}</p>
            {/* them vai gio hang */}
            <button
              className="btn btn-outline-info mr-2"
              onClick={() => this.props.addToCart(this.props.shoe)}
            >
              Add to cart <i className="bi bi-bag-heart-fill"></i>
            </button>
            {/* hien thi chi tiet */}
            <button
              className="btn btn-outline-secondary"
              onClick={() => {
                this.props.detail(this.props.shoe);
              }}
              data-toggle="modal"
              data-target="#myModal"
            >
              Detail <i className="bi bi-bag-heart-fill"></i>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (shoe) => {
      let action = {
        type: "ADD_TO_CART",
        payload: shoe,
      };
      dispatch(action);
    },
    detail:(shoe)=>{
      let action = {
        type: "DETAIL",
        payload:shoe,
      }
      dispatch(action);
    }
  };
};
export default connect(null,mapDispatchToProps)(ItemShoe)

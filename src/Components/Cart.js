import React, { Component } from "react";
import { connect } from "react-redux";

class Cart extends Component {
  renderTableCart = () => {
    return this.props.cart.map((item,index) => {
      return (
        <tr key={index}>
          <td>
            <img src={item.image} style={{ width: 50 }} />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              className="btn btn-outline-danger"
              onClick={() => {
                this.props.changeQuantity(item.id,-1);
              }}
            >
              <i className="bi bi-dash"></i>
            </button>
            <span className="mx-2">{item.soLuong}</span>
            <button
              className="btn btn-outline-success"
              onClick={() => {
                this.props.changeQuantity(item.id,1);
              }}
            >
              <i className="bi bi-plus"></i>
            </button>
          </td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              className="btn btn-outline-danger"
              onClick={() => {
                this.props.delete(item.id);
              }}
            >
              <i className="bi bi-trash3-fill"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <h2 className="text-info">Your Cart</h2>
        <table className="table table-striped" style={{fontSize:"1.5rem"}}>
          <thead>
            <tr>
              <td>Image</td>
              <td>Name</td>
              <td>Quantity</td>
              <td>Price</td>
              <td>Action</td>
            </tr>
          </thead>
          <tbody>{this.renderTableCart()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps=(state)=>{
  return {
    cart: state.shoesShopReducer.cart,
  }
}

let mapDispatchToProps=(dispatch)=>{
  return{
    delete:(id)=>{
      let action= {
        type:"DELETE",
        payload:id,
      }
      dispatch(action)
    },
    changeQuantity:(id,number)=>{
      let action= {
        type:"CHANGE",
        payload:{
          id,
          number,
        }
      }
      dispatch(action)
    }
  }
}

export default connect (mapStateToProps,mapDispatchToProps)(Cart)
import { data } from "../Components/Data";

const initialValue = {
  productsData: data,
  cart: [],
  detail: {},
};

export const shoesShopReducer = (state = initialValue, action) => {
  switch (action.type) {
    case "ADD_TO_CART": {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex(item=>{
        return item.id == action.payload.id;
      })
      if (index ==-1) {
        let newShoe={...action.payload,soLuong:1}
        cloneCart.push(newShoe)
      }else {
        cloneCart[index].soLuong++;
      }
      return{...state,cart:cloneCart}
    }
    case "CHANGE": {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex(item=>{
         return item.id==action.payload.id
      })
      if (index!=-1 ){
        if (cloneCart[index].soLuong==1 && action.payload.number == -1){
          cloneCart.splice(index,1)
        }else{
          cloneCart[index].soLuong+=action.payload.number
        }        
      }
      return{...state,cart:cloneCart}
    }
    case "DELETE": {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex(item=>{
        return item.id == action.payload
      })
      if (index!=-1){
        cloneCart.splice(index,1)
      }
      return{...state,cart:cloneCart}
    }
    case "DETAIL": {
      let cloneDetail={...action.payload}
      return{...state,detail:cloneDetail}
    }
    default:
      return state;
  }
};

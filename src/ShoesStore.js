import React, { Component } from "react";
import ProductList from "./Components/ProductList";
import Modal from "./Components/Modal";
import { data } from "./Components/Data";
import { connect } from "react-redux";

class ShoesStore extends Component {
  render() {
    return (
      <div>
        <ProductList
        ></ProductList>
        {this.props.detail != "" && <Modal></Modal>}
      </div>
    );
  }
}

let mapStateToProps = (state) =>{
  return {
    detail: state.shoesShopReducer.detail,
  }
}

export default connect (mapStateToProps)(ShoesStore)
